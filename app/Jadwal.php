<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $fillable = ['agenda', 'tanggal', 'waktu', 'tempat', 'tanggungjawab'];
}
