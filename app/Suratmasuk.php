<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suratmasuk extends Model
{
    protected $fillable = ['Kode_arsip', 'no_surat', 'nama_pengirim', 'tgl_masehi', 'tgl_hijriah', 'perihal', 'file'];
}
