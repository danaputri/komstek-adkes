<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anggota extends Model
{
    protected $fillable = ['nama', 'tempat_lahir', 'tgl_lahir', 'komisariat', 'tahun', 'sk_bpl'];
}
