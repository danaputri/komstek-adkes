<?php

namespace App\Exports;

use App\Suratkeluar;
use Maatwebsite\Excel\Concerns\FromCollection;

class SuratkeluarExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Suratkeluar::all();
    }
}
