<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\perpustakaan;

class PerpustakaanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $perpus = perpustakaan::all();
        return view('admin.Perpustakaan', compact('perpus'));
        // return view('admin.Perpustakaan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_Perpustakaan');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validatedData = $request->validate([
         'judul' => 'required',
         'penulis' => 'required',
         'penerbit' => 'required',
         'kategori' => 'required',
         'jumlah' => 'required',
         'kode' => 'required'
     ]);
     $perpus = perpustakaan::create($validatedData);

     return redirect('/Perpustakaan')->with('success', 'Data Perpustakaan is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $perpus = perpustakaan::findOrFail($id);

        return view('admin.edit_Perpustakaan', compact('perpus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $validatedData = $request->validate([
        'judul' => 'required',
         'penulis' => 'required',
         'penerbit' => 'required',
         'kategori' => 'required',
         'jumlah' => 'required',
         'kode' => 'required'
     ]);
        perpustakaan::whereId($id)->update($validatedData);

        return redirect('/Perpustakaan')->with('success', 'Data Perpustakaan is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $perpus = perpustakaan::findOrFail($id);
        $perpus->delete();

        return redirect('/Perpustakaan')->with('success', 'Data Perpustakaan is successfully deleted');
    }
}
