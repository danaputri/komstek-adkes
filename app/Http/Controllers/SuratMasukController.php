<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Suratmasuk;

class SuratMasukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
       $masuk = Suratmasuk::all();

     return view('admin.SuratMasuk', compact('masuk'));
        // return view('admin.SuratMasuk');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_Suratmasuk');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
         'Kode_arsip' => 'required',
         'no_surat' => 'required',
         'nama_pengirim' => 'required',
         'tgl_masehi' => 'required',
         'tgl_hijriah' => 'required',
         'perihal' => 'required',
         'file'=> 'required'
     ]);
     $masuk = Suratmasuk::create($validatedData);

     return redirect('/SuratMasuk')->with('success', 'Surat Masuk is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $masuk = Suratmasuk::findOrFail($id);

        return view('admin.edit_Suratmasuk', compact('masuk'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
        'Kode_arsip' => 'required',
         'no_surat' => 'required',
         'nama_pengirim' => 'required',
         'tgl_masehi' => 'required',
         'tgl_hijriah' => 'required',
         'perihal' => 'required',
         'file' => 'required'
        ]);
        Suratmasuk::whereId($id)->update($validatedData);

        return redirect('/SuratMasuk')->with('success', 'Surat Masuk is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $masuk = Suratmasuk::findOrFail($id);
        $masuk->delete();

        return redirect('/SuratMasuk')->with('success', 'Surat Masuk is successfully deleted');
    }
}
