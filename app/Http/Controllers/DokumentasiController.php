<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\dokumentasi;

class DokumentasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $dokumen = dokumentasi::all();
        return view('admin.Dokumentasi', compact('dokumen'));
        // return view('admin.Dokumentasi');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_Dokumentasi');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
         'foto' => 'required',
         'caption' => 'required',
         ]);
     $dokumen = dokumentasi::create($validatedData);

     return redirect('/Dokumentasi')->with('success', 'Dokumentasi Komstek is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dokumen = dokumentasi::findOrFail($id);

        return view('admin.edit_Dokumentasi', compact('dokumen'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
         'foto' => 'required',
         'caption' => 'required'
     ]);
        dokumentasi::whereId($id)->update($validatedData);

        return redirect('/Dokumentasi')->with('success', 'Dokumentasi Komstek is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dokumen = dokumentasi::findOrFail($id);
        $dokumen->delete();

        return redirect('/Dokumentasi')->with('success', 'Dokumentasi Komstek is successfully deleted');
    }
}
