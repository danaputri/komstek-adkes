<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Suratkeputusan;

class SKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $keputusan = Suratkeputusan::all();

     return view('admin.SK', compact('keputusan'));
        // return view('admin.SK');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_SK');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validatedData = $request->validate([
         'nomor_sk' => 'required',
         'tentang' => 'required',
         'tgl_masehi' => 'required',
         'tgl_hijriah' => 'required',
         'file' => 'required'
     ]);
     $keputusan = Suratkeputusan::create($validatedData);

     return redirect('/SuratKeputusan')->with('success', 'Surat Keputusan is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $keputusan = Suratkeputusan::findOrFail($id);

        return view('admin.edit_SK', compact('keputusan'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
         'nomor_sk' => 'required',
         'tentang' => 'required',
         'tgl_masehi' => 'required',
         'tgl_hijriah' => 'required',
         'file' => 'required'
     ]);
        Suratkeputusan::whereId($id)->update($validatedData);

        return redirect('/SuratKeputusan')->with('success', 'Surat keputusan is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $keputusan = Suratkeputusan::findOrFail($id);
        $keputusan->delete();

        return redirect('/SuratKeputusan')->with('success', 'Surat keputusan is successfully deleted');
    }
}
