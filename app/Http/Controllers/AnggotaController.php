<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Anggota;

class AnggotaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $anggota = Anggota::all();
        return view('admin.Anggota', compact('anggota'));
        // return view('admin.Anggota');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_Anggota');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validatedData = $request->validate([
         'nama' => 'required',
         'tempat_lahir' => 'required',
         'tgl_lahir' => 'required',
         'komisariat' => 'required',
         'tahun' => 'required',
         'sk_bpl' => 'required'
     ]);
     $anggota = Anggota::create($validatedData);

     return redirect('/Anggota')->with('success', 'Data Anggota is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $anggota = Anggota::findOrFail($id);

        return view('admin.edit_Anggota', compact('anggota'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
         'nama' => 'required',
         'tempat_lahir' => 'required',
         'tgl_lahir' => 'required',
         'komisariat' => 'required',
         'tahun' => 'required',
         'sk_bpl' => 'required'
     ]);
        Anggota::whereId($id)->update($validatedData);

        return redirect('/Anggota')->with('success', 'Data Anggota is successfully updated');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $anggota = Anggota::findOrFail($id);
        $anggota->delete();

        return redirect('/Anggota')->with('success', 'Data Anggota is successfully deleted');
    }
}
