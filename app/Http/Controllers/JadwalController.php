<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jadwal;

class JadwalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
         $jadwal = Jadwal::all();
        return view('admin.Jadwal', compact('jadwal'));
        // return view('admin.Jadwal');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_Jadwal');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $validatedData = $request->validate([
         'agenda' => 'required',
         'tanggal' => 'required',
         'waktu' => 'required',
         'tempat' => 'required',
         'tanggungjawab' => 'required'
         ]);
     $jadwal = Jadwal::create($validatedData);

     return redirect('/Jadwal')->with('success', 'Jadwal Agenda Komstek is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $jadwal = Jadwal::findOrFail($id);

        return view('admin.edit_Jadwal', compact('jadwal'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
         'agenda' => 'required',
         'tanggal' => 'required',
         'waktu' => 'required',
         'tempat' => 'required',
         'tanggungjawab' => 'required'
     ]);
        Jadwal::whereId($id)->update($validatedData);

        return redirect('/Jadwal')->with('success', 'Jadwal Agenda Komstek is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jadwal = Jadwal::findOrFail($id);
        $jadwal->delete();

        return redirect('/Jadwal')->with('success', 'Jadwal Agenda Komstek is successfully deleted');
    }
}
