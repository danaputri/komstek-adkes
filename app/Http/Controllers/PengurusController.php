<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengurus;

class PengurusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $pengurus = Pengurus::all();
        return view('admin.Pengurus', compact('pengurus'));
        // return view('admin.Pengurus');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_Pengurus');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
         'nama' => 'required',
         'tahun_lk' => 'required',
         'periode' => 'required',
         'jabatan' => 'required',
         'image' => 'required'

     ]);
        $gambar = $request->file('image');
        $new_name = rand() . '.' .$gambar->getClientOriginalExtension();
        $gambar->move(public_path('images'), $new_name);
        $form_data = array(
            'nama'       =>   $request->nama,
            'tahun_lk'        =>   $request->tahun_lk,
            'periode'        =>   $request->periode,
            'jabatan'        =>   $request->jabatan,
            'image'            =>   $new_name
        );
     
     $pengurus = Pengurus::create($validatedData);

     return redirect('/Pengurus')->with('success', 'Data Pengurus is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pengurus = Pengurus::findOrFail($id);

        return view('admin.edit_Pengurus', compact('pengurus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
        'nama' => 'required',
         'tahun_lk' => 'required',
         'periode' => 'required',
         'jabatan' => 'required',
         'image' => 'required'
     ]);
        Pengurus::whereId($id)->update($validatedData);

        return redirect('/Pengurus')->with('success', 'Data Pengurus is successfully updated');
         
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pengurus = Pengurus::findOrFail($id);
        $pengurus->delete();

        return redirect('/Pengurus')->with('success', 'Data Pengurus is successfully deleted');
    }
}
