<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Suratkeluar;
use App\Exports\SuratkeluarExport;
use Maatwebsite\Excel\Facades\Excel;

class SuratKeluarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $keluar = Suratkeluar::all();

     return view('admin.SuratKeluar', compact('keluar'));
        // return view('admin.SuratKeluar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.create_Suratkeluar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
         'kode_arsip' => 'required',
         'nomor_surat' => 'required',
         'tgl_masehi' => 'required',
         'tgl_hijriah' => 'required',
         'tujuan' => 'required',
         'perihal' => 'required',
         'file' => 'required'
     ]);
     $keluar = Suratkeluar::create($validatedData);

     return redirect('/SuratKeluar')->with('success', 'Surat Masuk is successfully saved');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $keluar = Suratkeluar::findOrFail($id);

        return view('admin.edit_Suratkeluar', compact('keluar'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
        'kode_arsip' => 'required',
         'nomor_surat' => 'required',
         'tgl_masehi' => 'required',
         'tgl_hijriah' => 'required',
         'tujuan' => 'required',
         'perihal' => 'required',
         'file' => 'required'
        ]);
        Suratkeluar::whereId($id)->update($validatedData);

        return redirect('/SuratKeluar')->with('success', 'Surat keluar is successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $keluar = Suratkeluar::findOrFail($id);
        $keluar->delete();

        return redirect('/SuratKeluar')->with('success', 'Surat keluar is successfully deleted');
    }

    public function export_excel()
    {
        return Excel::download(new SuratkeluarExport, 'suratkeluar.xls');
    }
}
