<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pengurus extends Model
{
    protected $fillable = ['nama', 'tahun_lk', 'periode', 'jabatan', 'image'];
}
