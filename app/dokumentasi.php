<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dokumentasi extends Model
{
    protected $fillable = ['foto', 'caption'];
}
