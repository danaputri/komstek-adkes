<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suratkeputusan extends Model
{
    protected $fillable = ['nomor_sk', 'tentang', 'tgl_hijriah', 'tgl_masehi', 'file'];
}
