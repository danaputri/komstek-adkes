<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suratkeluar extends Model
{
    protected $fillable = ['kode_arsip', 'nomor_surat', 'tgl_masehi', 'tgl_hijriah', 'tujuan', 'perihal', 'file'];
}
