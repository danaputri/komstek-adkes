@include('layouts.header')
<title>Ubah Data Pengurus</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/Pengurus">Data Pengurus</a>
          </li>
          <li class="breadcrumb-item active">Editor</li>
        </ol>
<div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
<form method="post" action="{{ route('Pengurus.update', $pengurus->id) }}">
    @csrf
    @method('PATCH')
          <div class="form-group">
              <label for="nama">Nama Pengurus</label>
              <input type="text" class="form-control" name="nama" value="{{$pengurus->nama}}" />
          </div>
          <div class="form-group">
              <label for="tahun_lk">Tahun LK 1</label>
              <input type="text" class="form-control" name="tahun_lk" value="{{$pengurus->tahun_lk}}" />
          </div>
          <div class="form-group">
              <label for="periode">Periode Kepengurusan</label>
              <input type="date" class="form-control" name="periode" value="{{$pengurus->periode}}" />
          </div>
          <div class="form-group">
              <label for="jabatan">Jabatan</label>
              <input type="text" class="form-control" name="jabatan" value="{{$pengurus->jabatan}}" />
          </div>
          <div class="form-group">
              <label for="image">Foto</label>
              <input type="file" class="form-control" name="image" value="{{$pengurus->image}}" />
          </div>
          <button type="submit" class="btn btn-primary">Update Data</button>
      </form>

@include('layouts.footer')