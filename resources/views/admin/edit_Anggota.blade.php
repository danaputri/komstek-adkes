@include('layouts.header')
<title>Ubah Data Anggota</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/Anggota">Data Anggota</a>
          </li>
          <li class="breadcrumb-item active">Editor</li>
        </ol>
<div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
<form method="post" action="{{ route('Anggota.update', $anggota->id) }}">
    @csrf
    @method('PATCH')
          <div class="form-group">
              <label for="nama">Nama Anggota</label>
              <input type="text" class="form-control" name="nama" value="{{$anggota->nama}}" />
          </div>
          <div class="form-group">
              <label for="tempat_lahir">Tempat Lahir</label>
              <input type="text" class="form-control" name="tempat_lahir" value="{{$anggota->tempat_lahir}}" />
          </div>
          <div class="form-group">
              <label for="tgl_lahir">Tanggal Lahir </label>
              <input type="date" class="form-control" name="tgl_lahir" value="{{$anggota->tgl_lahir}}" />
          </div>
          <div class="form-group">
              <label for="komisariat">Komisariat</label>
              <input type="text" class="form-control" name="komisariat" value="{{$anggota->komisariat}}" />
          </div>
          <div class="form-group">
              <label for="tahun">Tahun LK 1</label>
              <input type="text" class="form-control" name="tahun" value="{{$anggota->tahun}}" />
          </div>
          <div class="form-group">
              <label for="sk_bpl">No SK BPL</label>
              <input type="text" class="form-control" name="sk_bpl" value="{{$anggota->sk_bpl}}" />
          </div>
          <button type="submit" class="btn btn-primary">Update Data</button>
      </form>
@include('layouts.footer')