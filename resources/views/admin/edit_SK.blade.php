@include('layouts.header')
<title>Admin | Edit Surat Keputusan</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/SuratKeputusan">Surat Keputusan</a>
          </li>
          <li class="breadcrumb-item active">Editor</li>
        </ol>
<div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
<form method="post" action="{{ route('SuratKeputusan.update', $keputusan->id) }}">
    @csrf
    @method('PATCH')
          <div class="form-group">
              <label for="nomor_sk">Nomor Surat Keputusan</label>
              <input type="text" class="form-control" name="nomor_surat" value="{{$keputusan->nomor_sk}}" />
          </div>
          <div class="form-group">
              <label for="tentang">Tentang / Perihal </label>
              <input type="text" class="form-control" name="tujuan" value="{{$keputusan->tentang}}"/>
          </div>
          <div class="form-group">
              <label for="tgl_masehi">Tanggal Masehi </label>
              <input type="date" class="form-control" name="tgl_masehi" value="{{$keputusan->tgl_masehi}}"/>
          </div>
          <div class="form-group">
              <label for="tgl_hijriah">Tanggal Hijriah</label>
              <input type="text" class="form-control" name="tgl_hijriah" value="{{$keputusan->tgl_hijriah}}"/>
          </div>
          <button type="submit" class="btn btn-primary">Update Data</button>
      </form>
@include('layouts.footer')