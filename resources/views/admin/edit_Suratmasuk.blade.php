@include('layouts.header')
<title>Edit Surat Masuk</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Surat Masuk</a>
          </li>
          <li class="breadcrumb-item active">Editor</li>
        </ol>
<div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
<form method="post" action="{{ route('SuratMasuk.update', $masuk->id) }}">
    @csrf
    @method('PATCH')
          <div class="form-group">
              <label for="Kode_arsip">Kode Arsip :</label>
              <input type="text" class="form-control" name="Kode_arsip" value="{{$masuk->Kode_arsip}}" />
          </div>
          <div class="form-group">
              <label for="no_surat">Nomor Surat</label>
              <input type="text" class="form-control" name="no_surat" value="{{$masuk->no_surat}}" />
          </div>
          <div class="form-group">
              <label for="nama_pengirim">Nama Pengirim </label>
              <input type="text" class="form-control" name="nama_pengirim" value="{{$masuk->nama_pengirim}}" />
          </div>
          <div class="form-group">
              <label for="tgl_masehi">Tanggal Masehi </label>
              <input type="date" class="form-control" name="tgl_masehi" value="{{$masuk->tgl_masehi}}" />
          </div>
          <div class="form-group">
              <label for="tgl_hijriah">Tanggal Hijriah</label>
              <input type="text" class="form-control" name="tgl_hijriah" value="{{$masuk->tgl_hijriah}}" />
          </div>
          <div class="form-group">
              <label for="perihal">Perihal</label>
              <input type="text" class="form-control" name="perihal" value="{{$masuk->perihal}}" />
          </div>
          <button type="submit" class="btn btn-primary">Update Data</button>
      </form>
@include('layouts.footer')