@include('layouts.header')
<title>Admin | Jadwal</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/Jadwal">Jadwal</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>
<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Agenda</div>
          <div class="card-body">
            @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}  
              </div><br />
            @endif
            <a href="/Jadwal/create" type="button" class="btn btn-secondary">Tambah</a>
            <!-- <button class="btn btn-dark">Tambah</button> -->
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Agenda </th>
                    <th>Tanggal</th>
                    <th>Waktu</th>
                    <th>Tempat</th>
                     <th>tanggungjawab</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Agenda </th>
                    <th>Tanggal</th>
                    <th>Waktu</th>
                    <th>Tempat</th>
                     <th>tanggungjawab</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  @foreach($jadwal as $input)
                  <tr>
                    <td>{{$input->id}}</td>
                    <td>{{$input->agenda}}</td>
                    <td>{{$input->tanggal}}</td>
                    <td>{{$input->waktu}}</td>
                    <td>{{$input->tempat}}</td>
                    <td>{{$input->tanggungjawab}}</td>
                    <td>
                      <a href="{{ route('Jadwal.edit',$input->id)}}" class="btn btn-primary">Edit</a>
                      <form action="{{ route('Jadwal.destroy', $input->id)}}" method="post">
                        @csrf
                          @method('DELETE')
                      <button class="btn btn-danger" type="submit">Delete</button>
                </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
@include('layouts.footer')
@include('layouts.footer')