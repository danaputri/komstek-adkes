@include('layouts.header')
<title>Buat Dokumentasi</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/Dokumentasi">Dokumentasi</a>
          </li>
          <li class="breadcrumb-item active">Tambah Data</li>
        </ol>
<form method="post" action="{{ route('Dokumentasi.store') }}">
    @csrf
          
          <div class="form-group">
              <label for="foto">Foto</label>
              <input type="file" class="form-control" name="foto"/>
          </div>
          <div class="form-group">
              <label for="caption">Caption</label>
              <input type="text" class="form-control" name="caption"/>
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
      </form>
@include('layouts.footer')