@include('layouts.header')
<title>Tambah Data Anggota</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/Anggota">Anggota</a>
          </li>
          <li class="breadcrumb-item active">Tambah Data</li>
        </ol>
<form method="post" action="{{ route('Anggota.store') }}">
    @csrf
          
          <div class="form-group">
              <label for="nama">Nama Anggota</label>
              <input type="text" class="form-control" name="nama"/>
          </div>
          <div class="form-group">
              <label for="tempat_lahir">Tempat Lahir</label>
              <input type="text" class="form-control" name="tempat_lahir"/>
          </div>
          <div class="form-group">
              <label for="tgl_lahir">Tanggal Lahir </label>
              <input type="date" class="form-control" name="tgl_lahir"/>
          </div>
          <div class="form-group">
              <label for="komisariat">Komisariat</label>
              <input type="text" class="form-control" name="komisariat"/>
          </div>
          <div class="form-group">
              <label for="tahun">Tahun LK 1</label>
              <input type="text" class="form-control" name="tahun"/>
          </div>
          <div class="form-group">
              <label for="sk_bpl">No SK BPL</label>
              <input type="text" class="form-control" name="sk_bpl"/>
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
      </form>
@include('layouts.footer')