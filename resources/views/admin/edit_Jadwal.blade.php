@include('layouts.header')
<title>Ubah Data Agenda</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/Jadwal">Data Agenda</a>
          </li>
          <li class="breadcrumb-item active">Editor</li>
        </ol>
<div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
<form method="post" action="{{ route('Jadwal.update', $jadwal->id) }}">
    @csrf
    @method('PATCH')
          <div class="form-group">
              <label for="agenda">Nama Agenda</label>
              <input type="text" class="form-control" name="agenda" value="{{$jadwal->agenda}}" />
          </div>
          <div class="form-group">
              <label for="tanggal">Tanggal Pelaksanan</label>
              <input type="date" class="form-control" name="tanggal" value="{{$jadwal->tanggal}}" />
          </div>
          <div class="form-group">
              <label for="waktu">Waktu Pelaksanan</label>
              <input type="time" class="form-control" name="waktu" value="{{$jadwal->waktu}}"/>
          </div> 
          <div class="form-group">
              <label for="tempat">Tempat / Lokasi Pelaksanan</label>
              <input type="text" class="form-control" name="tempat" value="{{$jadwal->tempat}}"/>
          </div>
          <div class="form-group">
              <label for="tanggungjawab">Bidang PenanggungJawab</label>
              <input type="text" class="form-control" name="tanggungjawab" value="{{$jadwal->tanggungjawab}}"/>
          </div>
          <button type="submit" class="btn btn-primary">Update Data</button>
      </form>
@include('layouts.footer')