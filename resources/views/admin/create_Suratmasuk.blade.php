@include('layouts.header')
<title>Tambah Surat Masuk</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/SuratMasuk">Surat Masuk</a>
          </li>
          <li class="breadcrumb-item active">Tambah Data</li>
        </ol>
<form method="post" action="{{ route('SuratMasuk.store') }}">
    @csrf
          <div class="form-group">
              
              <label for="Kode_arsip">Kode Arsip :</label>
              <input type="text" class="form-control" name="Kode_arsip"/>
          </div>
          <div class="form-group">
              <label for="no_surat">Nomor Surat</label>
              <input type="text" class="form-control" name="no_surat"/>
          </div>
          <div class="form-group">
              <label for="nama_pengirim">Nama Pengirim </label>
              <input type="text" class="form-control" name="nama_pengirim"/>
          </div>
          <div class="form-group">
              <label for="tgl_masehi">Tanggal Masehi </label>
              <input type="date" class="form-control" name="tgl_masehi"/>
          </div>
          <div class="form-group">
              <label for="tgl_hijriah">Tanggal Hijriah</label>
              <input type="text" class="form-control" name="tgl_hijriah"/>
          </div>
          <div class="form-group">
              <label for="perihal">Perihal</label>
              <input type="text" class="form-control" name="perihal"/>
          </div>
          <div class="form-group">
              <label for="file">Scan Surat</label>
              <input type="file" class="form-control" name="file"/>
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
      </form>
@include('layouts.footer')