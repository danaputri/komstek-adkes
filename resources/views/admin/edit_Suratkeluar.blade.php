@include('layouts.header')
<title>Edit Surat Keluar</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Surat Keluar</a>
          </li>
          <li class="breadcrumb-item active">Editor</li>
        </ol>
<div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
<form method="post" action="{{ route('SuratKeluar.update', $keluar->id) }}">
    @csrf
    @method('PATCH')
          <div class="form-group">
              <label for="kode_arsip">Kode Arsip :</label>
              <input type="text" class="form-control" name="kode_arsip" value="{{$keluar->kode_arsip}}" />
          </div>
          <div class="form-group">
              <label for="nomor_surat">Nomor Surat</label>
              <input type="text" class="form-control" name="nomor_surat" value="{{$keluar->nomor_surat}}" />
          </div>
          <div class="form-group">
              <label for="tujuan">Tujuan </label>
              <input type="text" class="form-control" name="tujuan" value="{{$keluar->tujuan}}"/>
          </div>
          <div class="form-group">
              <label for="tgl_masehi">Tanggal Masehi </label>
              <input type="date" class="form-control" name="tgl_masehi" value="{{$keluar->tgl_masehi}}"/>
          </div>
          <div class="form-group">
              <label for="tgl_hijriah">Tanggal Hijriah</label>
              <input type="text" class="form-control" name="tgl_hijriah" value="{{$keluar->tgl_hijriah}}"/>
          </div>
          <div class="form-group">
              <label for="perihal">Perihal</label>
              <input type="text" class="form-control" name="perihal" value="{{$keluar->perihal}}"/>
          </div>
          <button type="submit" class="btn btn-primary">Update Data</button>
      </form>
@include('layouts.footer')