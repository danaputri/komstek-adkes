@include('layouts.header')
<title>Admin | Create Surat Keputusan</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/SuratKeputusan">Surat Keputusan</a>
          </li>
          <li class="breadcrumb-item active">Tambah Data</li>
        </ol>
<form method="post" action="{{ route('SuratKeputusan.store') }}">
    @csrf
          
          <div class="form-group">
              <label for="nomor_sk">Nomor Surat Keputusan</label>
              <input type="text" class="form-control" name="nomor_sk"/>
          </div>
          <div class="form-group">
              <label for="tentang">Tentang / Perihal </label>
              <input type="text" class="form-control" name="tentang"/>
          </div>
          <div class="form-group">
              <label for="tgl_masehi">Tanggal Masehi </label>
              <input type="date" class="form-control" name="tgl_masehi"/>
          </div>
          <div class="form-group">
              <label for="tgl_hijriah">Tanggal Hijriah</label>
              <input type="text" class="form-control" name="tgl_hijriah"/>
          </div>
          <div class="form-group">
              <label for="file">File Pdf / Scan Gambar</label>
              <input type="file" class="form-control" name="file"/>
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
      </form>
@include('layouts.footer')