@include('layouts.header')
<title>Admin | Perpustakaan</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/Perpustakaan">Perpustakaan</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>
<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Perpustakaan</div>
          <div class="card-body">
            @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}  
              </div><br />
            @endif
            <a href="/Perpustakaan/create" type="button" class="btn btn-secondary">Tambah</a>
            <!-- <button class="btn btn-dark">Tambah</button> -->
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Judul </th>
                    <th>Penulis</th>
                    <th>Penerbit</th>
                    <th>Kategori</th>
                     <th>Jumlah</th>
                    <th>Kode</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Judul </th>
                    <th>Penulis</th>
                    <th>Penerbit</th>
                    <th>Kategori</th>
                     <th>Jumlah</th>
                    <th>Kode</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  @foreach($perpus as $input)
                  <tr>
                    <td>{{$input->id}}</td>
                    <td>{{$input->judul}}</td>
                    <td>{{$input->penulis}}</td>
                    <td>{{$input->penerbit}}</td>
                    <td>{{$input->kategori}}</td>
                    <td>{{$input->jumlah}}</td>
                    <td>{{$input->kode}}</td>
                    <td>
                      <a href="{{ route('Perpustakaan.edit',$input->id)}}" class="btn btn-primary">Edit</a>
                      <form action="{{ route('Perpustakaan.destroy', $input->id)}}" method="post">
                        @csrf
                          @method('DELETE')
                      <button class="btn btn-danger" type="submit">Delete</button>
                </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
@include('layouts.footer')