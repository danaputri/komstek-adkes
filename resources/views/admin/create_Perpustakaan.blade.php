@include('layouts.header')
<title>Tambah Data Perpustakaan</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/Perpustakaan">Perpustakaan</a>
          </li>
          <li class="breadcrumb-item active">Tambah Data</li>
        </ol>
<form method="post" action="{{ route('Perpustakaan.store') }}">
    @csrf
          
          <div class="form-group">
              <label for="judul">Judul Buku</label>
              <input type="text" class="form-control" name="judul"/>
          </div>
          <div class="form-group">
              <label for="penulis">Nama Penulis</label>
              <input type="text" class="form-control" name="penulis"/>
          </div>
          <div class="form-group">
              <label for="penerbit">Nama Penerbit</label>
              <input type="text" class="form-control" name="penerbit"/>
          </div>
          <div class="form-group">
              <label for="kategori">Kategori Buku</label>
              <input type="text" class="form-control" name="kategori"/>
          </div>
          <div class="form-group">
              <label for="jumlah">Quantity</label>
              <input type="text" class="form-control" name="jumlah"/>
          </div>
          <div class="form-group">
              <label for="kode">Kode Buku</label>
              <input type="text" class="form-control" name="kode"/>
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
      </form>
@include('layouts.footer')