@include('layouts.header')
<title>Tambah Agenda</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/Jadwal">Jadwal</a>
          </li>
          <li class="breadcrumb-item active">Tambah Data</li>
        </ol>
<form method="post" action="{{ route('Jadwal.store') }}">
    @csrf
          
          <div class="form-group">
              <label for="agenda">Nama Agenda</label>
              <input type="text" class="form-control" name="agenda"/>
          </div>
          <div class="form-group">
              <label for="tanggal">Tanggal Pelaksanan</label>
              <input type="date" class="form-control" name="tanggal"/>
          </div>
          <div class="form-group">
              <label for="waktu">Waktu Pelaksanan</label>
              <input type="time" class="form-control" name="waktu"/>
          </div> 
          <div class="form-group">
              <label for="tempat">Tempat / Lokasi Pelaksanan</label>
              <input type="text" class="form-control" name="tempat"/>
          </div>
          <div class="form-group">
              <label for="tanggungjawab">Bidang PenanggungJawab</label>
              <input type="text" class="form-control" name="tanggungjawab"/>
          </div>                              
          <button type="submit" class="btn btn-primary">Submit</button>
      </form>
@include('layouts.footer')