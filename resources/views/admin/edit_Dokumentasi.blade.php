@include('layouts.header')
<title>Edit Dokumentasi</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/Dokumentasi">Dokumentasi</a>
          </li>
          <li class="breadcrumb-item active">Editor</li>
        </ol>
<div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
<form method="post" action="{{ route('Dokumentasi.update', $dokumen->id) }}">
    @csrf
    @method('PATCH')
          <div class="form-group">
              <label for="foto">Foto</label>
              <input type="file" class="form-control" name="foto" value="{{$dokumen->foto}}" />
          </div>
          <div class="form-group">
              <label for="caption">Caption</label>
              <input type="text" class="form-control" name="caption" value="{{$dokumen->caption}}" />
          </div>
          <button type="submit" class="btn btn-primary">Update Data</button>
      </form>

@include('layouts.footer')