@include('layouts.header')
<title>Tambah Surat Keluar</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/SuratKeluar">Surat Keluar</a>
          </li>
          <li class="breadcrumb-item active">Tambah Data</li>
        </ol>
<form method="post" action="{{ route('SuratKeluar.store') }}">
    @csrf
          <div class="form-group">
              <label for="kode_arsip">Kode Arsip :</label>
              <input type="text" class="form-control" name="kode_arsip"/>
          </div>
          <div class="form-group">
              <label for="nomor_surat">Nomor Surat</label>
              <input type="text" class="form-control" name="nomor_surat"/>
          </div>
          <div class="form-group">
              <label for="tujuan">Tujuan </label>
              <input type="text" class="form-control" name="tujuan"/>
          </div>
          <div class="form-group">
              <label for="tgl_masehi">Tanggal Masehi </label>
              <input type="date" class="form-control" name="tgl_masehi"/>
          </div>
          <div class="form-group">
              <label for="tgl_hijriah">Tanggal Hijriah</label>
              <input type="text" class="form-control" name="tgl_hijriah"/>
          </div>
          <div class="form-group">
              <label for="perihal">Perihal</label>
              <input type="text" class="form-control" name="perihal"/>
          </div>
          <div class="form-group">
              <label for="file">Scan Surat</label>
              <input type="file" class="form-control" name="file"/>
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
      </form>
@include('layouts.footer')