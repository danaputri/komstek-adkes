@include('layouts.header')
<title>Admin | Dokumentasi</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/Dokumentasi">Dokumentasi</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>
<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Table Example</div>
          <div class="card-body">
            @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}  
              </div><br />
            @endif
            <a href="/Dokumentasi/create" type="button" class="btn btn-secondary">Tambah</a>
            <!-- <button class="btn btn-dark">Tambah</button> -->
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <tbody>
                  @foreach($dokumen as $input)
                  <tr>
                    <td>{{$input->foto}}</td>
                    <td>{{$input->caption}}</td>
                    <td>
                      <a href="{{ route('Dokumentasi.edit',$input->id)}}" class="btn btn-primary">Edit</a>
                      <form action="{{ route('Dokumentasi.destroy', $input->id)}}" method="post">
                        @csrf
                          @method('DELETE')
                      <button class="btn btn-danger" type="submit">Delete</button>
                </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
@include('layouts.footer')