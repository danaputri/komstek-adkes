@include('layouts.header')
<title>Ubah Data Perpustakaan</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/Perpustakaan">Data Perpustakaan</a>
          </li>
          <li class="breadcrumb-item active">Editor</li>
        </ol>
<div class="card-body">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
<form method="post" action="{{ route('Perpustakaan.update', $perpus->id) }}">
    @csrf
    @method('PATCH')
    	<div class="form-group">
              <label for="judul">Judul Buku</label>
              <input type="text" class="form-control" name="judul" value="{{$perpus->judul}}" />
          </div>
          <div class="form-group">
              <label for="penulis">Nama Penulis</label>
              <input type="text" class="form-control" name="penulis" value="{{$perpus->penulis}}" />
          </div>
          <div class="form-group">
              <label for="penerbit">Nama Penerbit</label>
              <input type="text" class="form-control" name="penerbit" value="{{$perpus->penerbit}}" />
          </div>
          <div class="form-group">
              <label for="kategori">Kategori Buku</label>
              <input type="text" class="form-control" name="kategori" value="{{$perpus->kategori}}" />
          </div>
          <div class="form-group">
              <label for="jumlah">Quantity</label>
              <input type="text" class="form-control" name="jumlah" value="{{$perpus->jumlah}}" />
          </div>
          <div class="form-group">
              <label for="kode">Kode Buku</label>
              <input type="text" class="form-control" name="kode" value="{{$perpus->kode}}" />
          </div>
          <button type="submit" class="btn btn-primary">Update Data</button>
      </form>

@include('layouts.footer')