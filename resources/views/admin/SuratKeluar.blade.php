@include('layouts.header')
<title>Admin - Surat Keluar</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Surat Keluar</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>
<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Table Example</div>
          <div class="card-body">
            @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}  
              </div><br />
            @endif
            <a href="/SuratKeluar/create" type="button" class="btn btn-secondary">Tambah</a>
            <!-- <button class="btn btn-dark">Tambah</button> -->
            <a class="btn btn-warning" href="{{ route('exportsuratkeluar') }}">Export Surat Keluar</a>
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kode Arsip</th>
                    <th>Nomor Surat</th>
                    <th>Tanggal Masehi</th>
                    <th>Tanggal Hijriah</th>
                     <th>Tujuan</th>
                    <th>Perihal</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Kode Arsip</th>
                    <th>Nomor Surat</th>
                    <th>Tanggal Masehi</th>
                    <th>Tanggal Hijriah</th>
                     <th>Tujuan</th>
                    <th>Perihal</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  @foreach($keluar as $input)
                  <tr>
                    <td>{{$input->id}}</td>
                    <td>{{$input->Kode_arsip}}</td>
                    <td>{{$input->no_surat}}</td>
                    <td>{{$input->tgl_masehi}}</td>
                    <td>{{$input->tgl_hijriah}}</td>
                    <td>{{$input->tujuan}}</td>
                    <td>{{$input->perihal}}</td>
                    <td>
                      <a href="{{ route('SuratKeluar.edit',$input->id)}}" class="btn btn-primary">Edit</a>
                      <form action="{{ route('SuratKeluar.destroy', $input->id)}}" method="post">
                        @csrf
                          @method('DELETE')
                      <button class="btn btn-danger" type="submit">Delete</button>
                </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

@include('layouts.footer')