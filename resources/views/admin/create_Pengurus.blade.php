@include('layouts.header')
<title>Tambah Data Pengurus</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="/Pengurus">Pengurus</a>
          </li>
          <li class="breadcrumb-item active">Tambah Data</li>
        </ol>
<form method="post" action="{{ route('Pengurus.store') }}" enctype="multipart/form-data">
    @csrf
          
          <div class="form-group">
              <label for="nama">Nama Pengurus</label>
              <input type="text" class="form-control" name="nama"/>
          </div>
          <div class="form-group">
              <label for="tahun_lk">Tahun LK 1</label>
              <input type="text" class="form-control" name="tahun_lk"/>
          </div>
          <div class="form-group">
              <label for="periode">Periode Kepengurusan</label>
              <input type="date" class="form-control" name="periode"/>
          </div>
          <div class="form-group">
              <label for="jabatan">Jabatan</label>
              <input type="text" class="form-control" name="jabatan"/>
          </div>
          <div class="form-group">
              <label for="image">Foto</label>
              <input type="file" class="form-control" name="image"/>
          </div>
          <button type="submit" class="btn btn-primary">Submit</button>
      </form>
@include('layouts.footer')