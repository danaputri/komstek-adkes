@include('layouts.header')
<title>Admin | Data Anggota</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Data Anggota</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>
<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Table Example</div>
          <div class="card-body">
            @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}  
              </div><br />
            @endif
            <a href="/Anggota/create" type="button" class="btn btn-secondary">Tambah</a>
            <!-- <button class="btn btn-dark">Tambah</button> -->
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Anggota</th>
                    <th>Tempat Lahir</th>
                    <th>Tanggal Lahir</th>
                     <th>Komisariat</th>
                    <th>Tahun LK1</th>
                    <th>Nomor SK BPL</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama Anggota</th>
                    <th>Tempat Lahir</th>
                    <th>Tanggal Lahir</th>
                     <th>Komisariat</th>
                    <th>Tahun LK1</th>
                    <th>Nomor SK BPL</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  @foreach($anggota as $input)
                  <tr>
                    <td>{{$input->id}}</td>
                    <td>{{$input->nama}}</td>
                    <td>{{$input->tempat_lahir}}</td>
                    <td>{{$input->tgl_lahir}}</td>
                    <td>{{$input->komisariat}}</td>
                    <td>{{$input->tahun}}</td>
                    <td>{{$input->sk_bpl}}</td>
                    <td>
                      <a href="{{ route('Anggota.edit',$input->id)}}" class="btn btn-primary">Edit</a>
                      <form action="{{ route('Anggota.destroy', $input->id)}}" method="post">
                        @csrf
                          @method('DELETE')
                      <button class="btn btn-danger" type="submit">Delete</button>
                </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
@include('layouts.footer')