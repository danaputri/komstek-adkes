@include('layouts.header')
<title>Admin | Pengurus</title>
@include('layouts.navbar')
<!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="#">Pengurus</a>
          </li>
          <li class="breadcrumb-item active">Overview</li>
        </ol>
<!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Data Table Example</div>
          <div class="card-body">
            @if(session()->get('success'))
              <div class="alert alert-success">
                {{ session()->get('success') }}  
              </div><br />
            @endif
            <a href="/Pengurus/create" type="button" class="btn btn-secondary">Tambah</a>
            <!-- <button class="btn btn-dark">Tambah</button> -->
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama </th>
                    <th>Tahun LK1</th>
                    <th>Periode</th>
                    <th>Jabatan</th>
                     <th>Gambar</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Nama </th>
                    <th>Tahun LK1</th>
                    <th>Periode</th>
                    <th>Jabatan</th>
                     <th>Gambar</th>
                    <th>Action</th>
                  </tr>
                </tfoot>
                <tbody>
                  @foreach($pengurus as $input)
                  <tr>
                    <td>{{$input->id}}</td>
                    <td>{{$input->nama}}</td>
                    <td>{{$input->tahun_lk}}</td>
                    <td>{{$input->periode}}</td>
                    <td>{{$input->jabatan}}</td>
                    <td><img src="{{ URL::to('/') }}/images/{{ $input->image }}" class="img-thumbnail" width="75" /></td>
                    <td>
                      <a href="{{ route('Pengurus.edit',$input->id)}}" class="btn btn-primary">Edit</a>
                      <form action="{{ route('Pengurus.destroy', $input->id)}}" method="post">
                        @csrf
                          @method('DELETE')
                      <button class="btn btn-danger" type="submit">Delete</button>
                </form>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>
@include('layouts.footer')