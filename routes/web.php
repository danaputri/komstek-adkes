 <?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('SuratKeluar', 'SuratKeluarController');
Route::resource('SuratMasuk', 'SuratMasukController');
Route::resource('SuratKeputusan', 'SKController');
Route::resource('Anggota', 'AnggotaController');
Route::resource('Pengurus', 'PengurusController');
Route::resource('Dokumentasi', 'DokumentasiController');
Route::resource('Perpustakaan', 'PerpustakaanController');
Route::resource('Jadwal', 'JadwalController');
Route::resource('Info', 'InfoController');
Route::resource('Usermanagemen', 'UsermanagemenController');
Route::resource('Pesan', 'pesanController');

/*eksport ke excel*/

Route::get('exportsuratkeluar', 'SuratKeluarController@export_excel')->name('exportsuratkeluar');